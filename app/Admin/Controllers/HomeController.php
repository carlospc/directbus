<?php

namespace DirectBus\Admin\Controllers;

use DirectBus\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index()
    {

        // consulta
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.ic.peplink.com/rest/o?access_token=d22134a07e44d7d39ba9857b97e1bece');
        $resultado = $res->getBody();

        // render
        return Admin::content(function (Content $content) use($resultado) {

            $content->header('Panel de Control');
            //$content->description('Description...');

            $content->row(Dashboard::title());

            $content->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::environment());
                });

/*
                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::extensions());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::dependencies());
                });
*/                
            });

            $content->body($resultado);
        });
    }
}
